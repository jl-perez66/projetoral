<?php

function modelisme_setup()
{
    global $content_width;
    if ( ! isset( $content_width ) ) {
        $content_width = 1250; /* pixels */
    }

  
    add_theme_support( 'post-thumbnails' );

    $args = array(
        'default-image'      => get_template_directory_uri() . '/img/banniere.jpg',
        'default-text-color' => '000',
        'width'              => '196',
        'height'             => '51',
        'flex-width'         => true,
        'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'modelisme_setup' );

function modelisme_theme_name_script()
{
    wp_register_style('main_style',
        get_template_directory_uri().'/style.css', array(), true);
    wp_enqueue_style('main_style');
    
    wp_register_style('blog_style',
        get_template_directory_uri().'/css/blog.css', array(), true);
    wp_enqueue_style('blog_style');
    
    wp_register_style('meteo_style',
        get_template_directory_uri().'/meteo.css', array(), true);
    wp_enqueue_style('meteo_style');
    
    wp_register_style('bootstrap_style',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(),true);
    wp_enqueue_style('bootstrap_style');
   
    wp_register_script('mon_js',
        get_template_directory_uri().'/js/monscript.js');
    wp_enqueue_script('mon_js');
}

add_action('wp_enqueue_scripts', 'modelisme_theme_name_script');

function register_menu()
{
    register_nav_menus(
        array(
            'menu-sup' => __('Menu sup'),
            'menu-soc' => __('Menu social')
        )
    );
}
add_action('init', 'register_menu');

//gestion des widgets
function julien_widgets_init()
    {
        if ( function_exists('register_sidebar') ) {
            register_sidebar(array(
                'name' => __('sidebar latérale', 'julien'),
                'description' => __('zone de widget latérale', 'julien'),
                'id' => 'sidebar-1',
                'before_widget' => '<div class="border border-info mb-3 rounded p-2">',
                'after_widget' => '</div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>',
            ));

        }
    }
    add_action( 'widgets_init', 'julien_widgets_init' );



class modelisme_walker extends Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
            $output .= '<ul class="dropdown-menu">';
    }

    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0)
    {

        $title = $item->title;
        $description = $item->description;
        $permalink = $item->url;

        if($permalink != '#'){
            if($depth == 1){
               $output .= '';
            } else {

                $output .= "<li class='nav-item'>";
            }
        } else {
            $output .= "<li class='nav-item dropdown'>";
        }

        //Add SPAN if no Permalink
        if( $permalink && $permalink != '#' ) {
            if($depth == 1){
                $output .= '<a href="' . $permalink . '" class="dropdown-item">';
            } else {
                $output .= '<a href="' . $permalink . '" class="nav-link">';
            }
        } else {
                $output .= '<a href="' . $permalink .
                    '" class="nav-link dropdown-toggle" data-toggle="dropdown">';
        }

        $output .= $title;
        if( $description != '' && $depth == 0 ) {
            $output .= '<small class="description">' . $description . '</small>';
        }
        $output .= '</a>';
    }

    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        if($depth == 1){
            $output .='';
        }
    }

}





 


