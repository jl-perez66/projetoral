<?php get_header() ?>
<div class="container_accueil">
    <h1><?php bloginfo('name')?></h1>
    <i><h3>Découvrez de nouveaux horizons.</h3></i>
    
</div>
<?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
<div class="container_page_principal">
    <div class="page_title">
        <h1>Retrouvez votre sérénité ici.</h1>
    </div>
    <?php if ( is_active_sidebar( 'blog-sidebar' ) ) : ?>
 <div id="header-widget-area" class="nwa-header-widget widget-area" role="complementary">
 <?php dynamic_sidebar( 'blog-sidebar' ); ?>
 </div>
 <?php endif; ?>
    <p><?php the_content()?></p>
</div>
<?php endwhile; endif ?>
<?php get_footer(); ?>