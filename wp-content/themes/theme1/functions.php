<?php 

// Ajouter la prise en charge des images mises en avant
add_theme_support( 'post-thumbnails' );

// Ajouter automatiquement le titre du site dans l'en-tête du site
add_theme_support( 'title-tag' );

// Permet de créer des widgets
add_action('widgets_init', function(){
    register_sidebar(array (
    'name' => __('Sidebar THM 1', 'theme1'),
    'id' => 'blog-sidebar',
    'description' => __('Sidebar sur côté du blog', 'theme1'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2 class="widget-title"',
    'after_title' => '</h2>'
    
    ));
    register_sidebar(array(
        'name' => __('Sidebar THM 2', 'theme1'),
    'id' => 'blog-sidebar_bottom',
    'description' => __('Sidebar sur le bottom', 'theme1'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2 class="widget-title"',
    'after_title' => '</h2>'
    ));
    register_widget('wp_widget_meteo');
   add_action('widgets_init','wp_widget_meteo');
});

add_action('wp_enqueue_scripts', function(){
    wp_enqueue_script('jquery', get_template_directory_uri().'/bootstrap/jquery-3.4.1.min.js', array(), null, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/bootstrap/bootstrap.bundle.js', array('jquery'), null, true);
    wp_enqueue_style('bootstrap', get_template_directory_uri().'/bootstrap/bootstrap.css');
    wp_enqueue_style('style', get_template_directory_uri().'/style.css');
});

register_nav_menus( array(
    'main' => 'Menu principal',
    'footer' => 'Menu du bas'
) );

class wp_widget_meteo extends WP_Widget{

    public function __construct()
    {
        $widget_opts = [
            'classname' => 'widget_meteo',
            'description' => __('Le fameux widget météo !!'),
            'customize_selective_refresh' => true,
        ];

        parent::__construct('widget_meteo',__('Meteo Widget','widget_meteo'),$widget_opts);
    }

    public function form($instance)
    {

        $instance = wp_parse_args( (array)$instance,
            array('lat'=>'', 'lon'=>'', 'apiToken'=>'') );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('lat') ?>">Lattitude</label>
            <input class="widefat" id="<?php echo $this->get_field_id('lat') ?>"
              type="text" name="<?php echo $this->get_field_name('lat') ?>"
                value="<?php echo esc_attr($instance['lat']) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('lon') ?>">Longitude</label>
            <input class="widefat" id="<?php echo $this->get_field_id('lon') ?>"
              type="text" name="<?php echo $this->get_field_name('lon') ?>"
                value="<?php echo esc_attr($instance['lon']) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('apiToken') ?>">Api Dark Sky</label>
            <input class="widefat" id="<?php echo $this->get_field_id('apiToken') ?>"
              type="text" name="<?php echo $this->get_field_name('apiToken') ?>"
                value="<?php echo esc_attr($instance['apiToken']) ?>">
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {

        $intance = $old_instance;
        $intance['lat'] = sanitize_text_field($new_instance['lat']);
        $intance['lon'] = sanitize_text_field($new_instance['lon']);
        $intance['apiToken'] = sanitize_text_field($new_instance['apiToken']);

        return $intance;
    }

    public function widget($args, $instance)
    {
        $title = 'Méteo';
        // définition de l'url d'appel de l'api
        $url = 'https://api.darksky.net/forecast/'.$instance['apiToken']
            .'/'.$instance['lat'].','.$instance['lon'];
        // appel de l'api
        $request = wp_remote_get($url);
        if( is_wp_error( $request ) ) { // si il y a une erreur
            return false; // Bail early
        }
        // on récupère le body de la réponse
        $body = wp_remote_retrieve_body( $request );
        // on parse le body pour transformer le json en tableau
        $data = json_decode( $body );
        echo $args['before_widget'];
        if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<div id="meteo_wrap" class="meteo_wrap">';
        if( ! empty( $data ) ){

            echo '<h1> La météo aujourd\'hui </h1>';
            echo '<div> La température aujourd\'hui est à : '.
                number_format((($data->currently->temperature -32) * (5/9)),
                    2,',',' ').
                ' degrés</div>';
            echo '<div> La vitesse du vent aujourd\'hui : '.
                number_format(($data->currently->windSpeed * 1.609),
                    2,',',' ').
                ' km</div>';

        }
        echo '</div>';
        echo $args['after_widget'];

        return '';
    }
}