<div class="newsletter">
<p>Pour vous tenir au courant des actualités, du contenu bonus et des réductions, <strong>abonnez-vous !</strong></p>
<form class="newsletter-form">
    <input type="email" name="email" placeholder="Votre e-mail">
    <button type="submit" class="btn btn-dark">S'abonner !</button>
</form>
</div>