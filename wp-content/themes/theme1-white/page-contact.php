<?php get_header() ?>
<div class="container_contact">
    <h1><?php bloginfo('name')?></h1>
    <i><h3>Page de <?php the_title();?></h3></i>
    
</div>
<?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
<div class="container_page_principal">
    <div class="page_title">
        <h1>Contactez-nous !</h1>
    </div>
    <p><?php the_content()?></p>
</div>
<form>
    <div class="form-group">
        <label for="email">Votre mail</label>
        <input type="email" class="form-control" id="email" aria-describedby="email_help" placeholder="Entrez votre mail ici">
        <small id="email_help" class="form-text text-muted">Votre adresse nous permettra de vous contacter.</small>
    </div>
    <div class="form-group">
        <label for="message">Votre message</label>
       <textarea class="form-control" id="message1" placeholder="Votre message" aria-describedby="message_help"></textarea>
        <small id="message_help" class="form-text text-muted">Votre message sera lu sous peu de temps.</small>
        <button type="submit" class="btn btn-dark">Envoyer !</button>
    </div>
</form>
<?php endwhile; endif ?>
<?php get_footer(); ?>