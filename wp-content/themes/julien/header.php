<html lang="<?php language_attributes(); ?>">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/icone.png" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
    
    <?php  wp_head();?>
</head>
<body>

<div class="logo">
    <a href="<?php echo home_url(); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/img/banniere.jpg" alt="Logo" class="logo-img">
    </a>
</div>

<div class="container">
    
    <!-- menu nav bar -->
    <nav class="navbar navbar-expand-lg navbar-light ">
        <!-- menu hamburger -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" 
        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
        aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <?php


        wp_nav_menu(array(
        'theme_location' => 'menu-sup',
        'container'       => 'div',
        'container_class' => 'collapse navbar-collapse',
        'container_id'    => 'navbarSupportedContent',
        'menu_class' => 'navbar-nav mr-auto',
        'menu_id' => ' ',
        'walker' => new modelisme_walker(),
        ));
        ?>
    </nav>

