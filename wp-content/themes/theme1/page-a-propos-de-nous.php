<?php get_header() ?>
<div class="container_contact">
    <h1><?php bloginfo('name')?></h1>
    <i><h3><?php the_title();?></h3></i>
    
</div>
<?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
<div class="container_page_principal">
    <div class="page_title">
        <h1>Mais, qui vous êtes?</h1>
    </div>
    <p><?php the_content()?></p>
</div>

<?php endwhile; endif ?>
<?php get_footer(); ?>