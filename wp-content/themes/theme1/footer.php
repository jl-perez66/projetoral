<footer>
    <?php if ( is_active_sidebar( 'blog-sidebar_bottom' ) ) : ?>
     <div id="header-widget-area" class="nwa-header-widget widget-area" role="complementary">
     <?php dynamic_sidebar( 'blog-sidebar_bottom' ); ?>
     </div>
     <?php endif; ?>
    <?php wp_footer(); ?>
<?php wp_nav_menu(array('theme_location' => 'footer','container'=>'ul','menu_class'=>'site__footer__menu') ); ?>
</footer>